(function() {
    //SlideShow Implementation
    var doc = document,
        carousel = doc.querySelector('.carousel'),
        images = doc.querySelectorAll('img'),
        rightBtn = doc.querySelector('.carousel-control-right'),
        leftBtn = doc.querySelector('.carousel-control-left'),
        counter = 0;

    //Init
    loadSlideShow(counter);

    //Events
    rightBtn.addEventListener("click", onRightBtnClicked);
    leftBtn.addEventListener("click", onleftBtnClicked);
    window.setInterval(onRightBtnClicked, 3000);

    //Functions
    function loadSlideShow(itemPos) {
        for (var i = 0; i < images.length; i += 1) {
            images[i].className = 'img-hide';
        }
        images[itemPos].className = 'img-show';
    }

    function onRightBtnClicked() {
        (counter < images.length - 1) ? counter += 1: counter = 0;
        loadSlideShow(counter);
    }

    function onleftBtnClicked() {
        (counter > 0) ? counter -= 1: (counter = images.length - 1);
        loadSlideShow(counter);
    }

})();
